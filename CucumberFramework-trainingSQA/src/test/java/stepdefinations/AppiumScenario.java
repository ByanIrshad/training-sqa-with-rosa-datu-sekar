package stepdefinations;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utility.Hook;

public class AppiumScenario {

private WebDriver driver;
	private AndroidDriver<MobileElement> driverAndroid;
	private MobileElement element;
	
	public AppiumScenario() {
		this.driver = Hook.getDriver();
	}
	
	@Given("^I open the application$")
	public void i_open_the_application() throws Throwable {
	    Assert.assertTrue(driver.findElement(By.xpath("//*[@resource-id='com.google.android.googlequicksearchbox:id/googleapp_logo']")).isDisplayed());
	}

	@Then("^I validate Custom View$")
	public void i_validate_Custom_View() throws Throwable {
//	  Assert.assertTrue(driver.findElement(By.xpath("//*[@text='Custom View']")).isDisplayed(), "Custom View is not displayed");
	  
	}

	@Given("^I open Youtube$")
	public void iOpenYoutube() throws Exception {
		Thread.sleep(5000);
		Assert.assertTrue(driver.findElement(By.xpath("//*[@resource-id='com.google.android.youtube:id/youtube_logo']")).isDisplayed());
	}

	@When("^I search \"([^\"]*)\"$")
	public void iSearch(String cari) throws Exception {
		// Write code here that turns the phrase above into concrete actions
		MobileElement fieldSearch = (MobileElement) driver.findElement(By.xpath("//android.widget.ImageView[@content-desc=\"Search\"]"));
		fieldSearch.click();
		Thread.sleep(2000);
		MobileElement textSearch = (MobileElement) driver.findElement(By.xpath("//android.widget.EditText[@resource-id=\"com.google.android.youtube:id/search_edit_text\"]"));
		textSearch.sendKeys(cari);
		MobileElement listsearch = (MobileElement) driver.findElement(By.xpath("//android.view.ViewGroup[@resource-id=\"com.google.android.youtube:id/linearLayout\"][1]"));
		Thread.sleep(2000);
		listsearch.click();
		Thread.sleep(5000);
		throw new PendingException();
	}

	@Then("^Validate Search Youtube$")
	public void validateSearchYoutube() throws Exception {
		Thread.sleep(5000);
		Assert.assertTrue(driver.findElement(By.xpath("//*[@content-desc=\"Subscribe to Cocomelon - Nursery Rhymes.\"]")).isDisplayed());
	}
}
