package stepdefinations;

import java.awt.*;
import java.util.concurrent.TimeUnit;

import cucumber.api.PendingException;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import utility.Hook;


public class InstagramScenario {

    private final WebDriver driver;

    public InstagramScenario() {
        this.driver = Hook.getDriver();
    }

    @Given("^I navigate to \"([^\"]*)\"$")
    public void inavigateto(String link) throws Throwable {
        driver.get("https://"+link);
        Thread.sleep(2000);
    }

    @When("^I login with ID \"([^\"]*)\" Password \"([^\"]*)\"$")
    public void iLoginWithIDPassword(String id, String password) throws Throwable {
        driver.findElement(By.name("username")).sendKeys(id);
        Thread.sleep(2000);
        driver.findElement(By.name("password")).sendKeys(password);
        Thread.sleep(2000);
//        Assert.assertTrue(driver.findElements(By.xpath("//*[@role=\"alert\"]").d);

    }

    @And("^i click Log In$")
    public void iClickLogIn() throws Throwable{
        WebElement loginButton = driver.findElement(By.xpath("//button[@type='submit']"));
        loginButton.click();

    }

    @Then("^I validate instagram login$")
    public void iValidateInstagramLogin() throws Exception {

//        Assert.assertTrue(driver.findElement(By.name("Reels")).isDisplayed());
    }

    @When("^I upload a photo with caption \"([^\"]*)\"$")
    public void i_upload_a_photo_with_caption(String caption) throws InterruptedException, AWTException {
        Thread.sleep(6000);
// Click on the "Create" button
        WebElement newPostButton = driver.findElement(By.xpath("//div[contains(text(), 'Create')]"));
        newPostButton.click();

// Select the photo to upload
        WebElement fileInput = driver.findElement(By.xpath("//input[@type='file']"));
        Thread.sleep(5000);
        fileInput.sendKeys(System.getProperty("user.dir") + "/image.png");

// Click next
        WebElement nextCrop = driver.findElement(By.xpath("//div[contains(text(), 'Next')]"));
        nextCrop.click();
        Thread.sleep(6000);
        WebElement nextEdit = driver.findElement(By.xpath("//div[contains(text(), 'Next')]"));
        nextEdit.click();
        Thread.sleep(6000);

// Enter the caption
        WebElement captionInput = driver.findElement(By.xpath("//div[@aria-label='Write a caption...']"));
        captionInput.sendKeys(caption);

// Click the "Share" button
        WebElement shareButton = driver.findElement(By.xpath("//div[contains(text(),'Share')]"));
        shareButton.click();
    }

    @Then("^I should see the uploaded photo in my Instagram feed$")
    public void i_should_see_the_uploaded_photo_in_my_Instagram_feed() throws InterruptedException {
        Thread.sleep(5000);
// Navigate to the Instagram feed
        driver.get("https://www.instagram.com/trialbcad");

// Verify that the photo is in the feed
        WebElement uploadedPhoto = driver.findElement(By.xpath("//div[@class='_aagu']"));
        uploadedPhoto.click();

        WebElement captionPhoto = driver.findElement(By.xpath("(//h1[normalize-space()='trial'])[1]"));
        Assert.assertEquals(captionPhoto.getText(), "test");
    }
}
