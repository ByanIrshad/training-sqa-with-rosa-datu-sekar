#Author: freeautomationlearning@gmail.com
@instagram
Feature: Instagram

  Background:
    Given I open a browser


  Scenario: Upload Instagram
    Given I navigate to "www.instagram.com"
    When I login with ID "trialbcad" Password "@29Maret2023"
    And i click Log In
    Then I validate instagram login
    When I upload a photo with caption "hiha"
    Then I should see the uploaded photo in my Instagram feed

